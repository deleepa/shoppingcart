<?php

/* The user arrives at this page from checkout.php
 * The user had an option to save their shipping information on that page. 
 * 1. If that option has been checked, save the information and proceed to payment
 * 2. Else go directly to payment
 */

    if($_SERVER['REQUEST_METHOD'] == "POST") {
        //the user has requested to save their information
        if(array_key_exists('saveCustomerInformation', $_POST)) {
            require_once './includes/db.php';
            $connection = ShoppingCartDB::getInstance();
            session_start();
            
            $result = $connection->insertShippingDetails($_POST['streetAddress'], $_POST['suburb'], $_POST['city'], $_POST['postcode'], $_POST['country'], $_SESSION['userId']);
            
            if($result) {
                $dbResult = "true";
            } 
            else {
                $dbResult = "false";
            }
            
            header("Location: paymentDetails.php?save=".$dbResult);
        }
        //the user has not requested to save their information
        else {
            header("Location: paymentDetails.php?save=null");
        }
    }
    else {
        header("Location: index.php");
    }

?>
