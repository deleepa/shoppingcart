<?php
    /*This php page will process the user's information
     * and forward them to the eway payment page. Once their 
     * payment has been confirmed, they will be returned to the set 
     * url
     */
    if($_SERVER['REQUEST_METHOD'] == "POST") {
               
        // Eway payment in PHP
        $returnURL="http://localhost:8080/ShoppingCart/eway/ewayresponse.php";
        $cancelURL="http://localhost:8080/ShoppingCart/paymentConfirmation.php";
        if(array_key_exists('action', $_GET) && $_GET['action']=="payment")
        {
            $ewayurl = "";
            $ewayurl.="?CustomerID=87654321";
            $ewayurl.="&UserName=TestAccount";
            //TODO: the total needs to be adjusted to .00
            $ewayurl.="&Amount=".sprintf("%.2f",$_POST['total']);
            $ewayurl.="&Currency=AUD";
            $ewayurl.="&PageTitle=";
            $ewayurl.="&PageDescription=";
            $ewayurl.="&PageFooter=";	
            $ewayurl.="&Language=";
            $ewayurl.="&CompanyName=";
            $ewayurl.="&CustomerFirstName=".$_POST['firstName'];
            $ewayurl.="&CustomerLastName=".$_POST['lastName'];		
            $ewayurl.="&CustomerAddress=".$_POST['streetAddress']." ".$_POST['suburb'];
            $ewayurl.="&CustomerCity=".$_POST['city'];
            $ewayurl.="&CustomerState=".$_POST['state'];
            $ewayurl.="&CustomerPostCode=".$_POST['postcode'];
            $ewayurl.="&CustomerCountry=".$_POST['country'];		
            $ewayurl.="&CustomerEmail=".$_POST['email'];
            $ewayurl.="&CustomerPhone=".$_POST['email'];		
            $ewayurl.="&InvoiceDescription=";
            $ewayurl.="&CancelURL=".$cancelURL;
            $ewayurl.="&ReturnUrl=".$returnURL;
            $ewayurl.="&CompanyLogo=";
            $ewayurl.="&PageBanner=";
            $ewayurl.="&MerchantReference=";
            $ewayurl.="&MerchantInvoice=";
            $ewayurl.="&MerchantOption1="; 
            $ewayurl.="&MerchantOption2=";
            $ewayurl.="&MerchantOption3=";
            $ewayurl.="&ModifiableCustomerDetails=";

            $spacereplace = str_replace(" ", "%20", $ewayurl);	
            $posturl="https://au.ewaygateway.com/Request/$spacereplace";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $posturl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);


            define('CURL_PROXY_REQUIRED', 'false');
            define('CURL_PROXY_SERVER_DETAILS', 'localhost:3128');
            if (CURL_PROXY_REQUIRED == 'True') 
            {
                    $proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
                    curl_setopt ($ch, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
                    curl_setopt ($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
                    curl_setopt ($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
            }

            $response = curl_exec($ch);

            function fetch_data($string, $start_tag, $end_tag)
            {
                    $position = stripos($string, $start_tag);  
                    $str = substr($string, $position);  		
                    $str_second = substr($str, strlen($start_tag));  		
                    $second_positon = stripos($str_second, $end_tag);  		
                    $str_third = substr($str_second, 0, $second_positon);  		
                    $fetch_data = trim($str_third);		
                    return $fetch_data; 
            }


            $responsemode = fetch_data($response, '<result>', '</result>');
            $responseurl = fetch_data($response, '<uri>', '</uri>');

            if($responsemode=="True")
            { 			  	  	
              header("location: ".$responseurl);
              exit;
            }
            else
            {
              header("location: error.php");
              exit; 
            }
        }
    }
    else {
        header("Location: index.php");
    }
?>
