<?php
    /* If the transaction is sucessful, the order must be updated to ordered
     * and the customer id inserted as well.
     */
    //paymentConfirmation.php?trxstatus=<?php print$trxnstatus;&trxnum=
    session_start();
    $transactionNumber = "";
    $transactionSucessful = false;
    if(array_key_exists('trxstatus', $_GET)) {
        if($_GET['trxstatus'] == "True"){
            $transactionSucessful = TRUE;
            $transactionNumber = $_GET['trxnum'];
            
            require_once './includes/db.php';
            $connection = ShoppingCartDB::getInstance();

            $result = $connection->updateOrderDetails($_SESSION['orderId'], "ordered", $_SESSION['userId']);
        }       
        else if($_GET['trxstatus'] == 'False') {
            $transactionSucessful = FALSE;
            $transactionNumber = $_GET['trxnum'];
        }
    }    
    else {
        header("Location: index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="style.css" />
        <title></title>
    </head>
    <body>
        <h1>Payment confirmation</h1>
        <?php if($transactionSucessful) { ?>
        <div id="paymentConfirmation_confimed">
            Your order is now confirmed. <br />
            Order number: <?php print $_SESSION['orderId'];?> <br />
            Transaction number: <?php print $transactionNumber;?> <br />
            <a href="index.php">Back to main page</a>
            <?php unset($_SESSION['orderId'])?>
        </div>
        <?php } 
              else{
        ?>
        <div id="paymentConfirmation_error">
            Unfortunately there was an error processing your transaction. <br />
            Your credit card has NOT been charged. <br />
            Save the following details and contact us by email or phone to rectify the situation. <br />
            Order number: <?php print $_SESSION['orderId'];?> <br />
            Transaction number: <?php print $transactionNumber;?> <br />
            
            <a href="index.php">Back to main page</a>
            <?php unset($_SESSION['orderId'])?>
        </div>        
        <?php
              }
        ?>
    </body>
</html>
