<?php
    /* In this page, the user will create a new account
     * The program must carry out the following validations:
     * 1. There cannot be any empty fields
     * 2. The password must match the confirm password
     * 
     * Then the program must enter the new customer, store the userId
     * in the session and redirect them to the index page 
     */
    $passwordInvalid = false;
    $userNameInvalid = false;
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        if(array_key_exists('createNewAccount', $_POST)){
            
            $password = $_POST['password'];
            $confirmPassword = $_POST['confirmPassword'];
            
            if($password != $confirmPassword) {
                $passwordInvalid = true;
            }
            else {
                require_once './includes/db.php';
                $connection = ShoppingCartDB::getInstance();
                
                $result = $connection->createUserNewAccount($_POST['username'], $password, $_POST['firstName'], $_POST['lastName']);
                //$result = true;
                if($result) {
                    session_start();
                    $_SESSION['userId'] = $connection->getUserIdByUsername($_POST['username']);
                    header("Location: index.php?user=" . $_POST['firstName']);
                }
                else {
                    $userNameInvalid = true;
                }
            }
            
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="style.css">
        <title></title>
    </head>
    <body>
        <h1>New Account Registration</h1>
        <?php
            if($userNameInvalid) {
                print "<div id=\"newAccount_creationError\">Sorry. That username has already been taken.</div>";
            }
        ?>
        <div id="newAccount_compulsory">
            <form method="POST" action="#">
                <label>Username</label><input type="text" name="username" required="" /> <br />
                <label>Password</label><input type="password" name="password" required="" /> <br />
                <label>Confirm password</label><input type="password" name="confirmPassword" required="" /> <br />
                <?php if($passwordInvalid) echo '<label id="newAccount_invalidPassword">Your passwords did not match</label> <br />';?>
                <label>First name</label><input type="text" name="firstName" required="" /> <br />
                <label>Last name</label><input type="text" name="lastName" required="" /> <br />
                <input id="newAccount_submit" type="submit" name="createNewAccount" value="Create My Account" />
            </form>
        </div>
        
    </body>
</html>
