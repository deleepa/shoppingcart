<?php
    if($_SERVER['REQUEST_METHOD'] = "POST") {
        
        session_start();
        require_once './includes/db.php';
        $connection = ShoppingCartDB::getInstance();
        $userInfo = "";
        $addressInfo = "";
        $orders = "";
        
        if(array_key_exists('userId', $_SESSION)) {
            $userInfo = $connection->getUserDetailsById($_SESSION['userId']);
            $orders = $connection->getOrdersByUserId($_SESSION['userId']);
            if($userInfo['shipping_address_id'] != null) {
                $addressInfo = $connection->getAddressDetailsById($userInfo['shipping_address_id']);
            }
            
        }
    }
    else {
        header("Location: index.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="style.css" />
        <title></title>
    </head>
    <body>
        <h1>User Portal</h1>
        <div id="userPortal_menu">
            <ul> 
                <li>
                    <form action="cart.php?user=<?php print $userInfo['first_name']?>" method="POST">
                        <input type="submit" value="My Cart" name="myCart" />
                    </form>
                </li>
                <li>
                    <form action="index.php" method="POST">
                        <input type="hidden" value="<?php print $userInfo['first_name'];?>" name="userName" />
                        <input type="submit" value="Gallery" name="gallery" />
                    </form>
                </li>
            </ul>
        </div>
        <div id="userPortal_welcomeMessage">
            Welcome <?php print $userInfo['first_name'];?>
        </div>
        <div id="userPortal_pendingOrders">
            Pending orders <br />
            <?php
                if($orders != null) {
                    for($i = 0; $i < $orders->num_rows; $i++) {
                        $row = mysqli_fetch_array($orders, MYSQLI_ASSOC);
                        print "Order Number: " . $row['id'] . "<br />";
                        print "Status: " . $row['status'] . "<br /> <br />";
                    }
                }
            ?>
        </div>
        <div id="userPortal_userDetails">
            User details <br />
            Username: <?php print $userInfo['username'];?> <br />
            First name: <?php print $userInfo['first_name'];?> <br />
            Last name: <?php print $userInfo['last_name'];?> <br /><br />
            
            Delivery details <br />
            Street Address: <?php if($addressInfo!=null) print $addressInfo['street_address'];?> <br />
            Suburb: <?php if($addressInfo!=null) print $addressInfo['suburb'];?> <br />
            City: <?php if($addressInfo!=null) print $addressInfo['city'];?> <br />
            Country: <?php if($addressInfo!=null) print $addressInfo['country'];?> <br />
            
        </div>
        
    </body>
</html>
