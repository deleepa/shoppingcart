<?php
    session_start();
    $userInfo = "";
    $shippingInfo = "";
    if(array_key_exists('userId', $_SESSION)) {
        require_once 'includes/db.php';
        $connection = ShoppingCartDB::getInstance();
        $userInfo = $connection->getUserDetailsById($_SESSION['userId']);
        $shippingInfo = $connection->getAddressDetailsById($userInfo['shipping_address_id']);
        
        
    }
    else {
        header("Location: index.php");
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="style.css" />
        <title></title>
    </head>
    <body>
        <h1>Payment Details</h1>
        <div id="paymentDetails_informationBar">
            Important Note:
            The information you enter below is being sent to eWay who will then
            proceed to process your transaction. At no point do we save any information
            you enter here.
        </div>
        <div id="paymentDetails_form">
            <form method="POST" action="sendToEway.php?action=payment">
                <label>First Name</label><input type="text" name="firstName" required="" value="<?php print $userInfo['first_name'];?>" /> <br />
                <label>Last Name</label><input type="text" name="lastName" required="" value="<?php print $userInfo['last_name'];?>" /> <br />
                <label>Email</label><input type="text" name="email" required="" value="" /> <br />
                <label>Phone</label><input type="text" name="phone" required="" value="" /> <br />
                <label>Street Address</label><input type="text" name="streetAddress" required="" value="<?php if($shippingInfo!=NULL) print $shippingInfo['street_address'];?>"/> <br />
                <label>Suburb</label><input type="text" name="suburb" required="" value="<?php if($shippingInfo!=NULL) print $shippingInfo['suburb'];?>"/> <br />
                <label>City</label><input type="text" name="city" required="" value="<?php if($shippingInfo!=NULL) print $shippingInfo['city'];?>"/> <br />
                <label>State</label><input type="text" name="state" required="" value=""/> <br />
                <label>Postcode</label><input type="text" name="postcode" required="" value="<?php if($shippingInfo!=NULL) print $shippingInfo['postcode'];?>"/> <br />
                <label>Country</label><input type="text" name="country" required="" value="<?php if($shippingInfo!=NULL) print $shippingInfo['country'];?>"/> <br />
                <label id="paymentDetails_total">Total due: $<?php print $_SESSION['total'];?></label>
                <input type="hidden" name="total" value="<?php print $_SESSION['total'];?>.00">
                <input id="paymentDetails_submit" type="submit" name="continueToPayment" value="Continue to payment" />
            </form>
        </div>
    </body>
</html>
