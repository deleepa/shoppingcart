<?php
    session_start();
    $userName = NULL;
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        //var_dump($_POST);
        if(array_key_exists('logOut', $_POST)) {
            session_destroy();
            header("Location: index.php");
        }   
    }
    
    if(($_SERVER['REQUEST_METHOD'] == "POST") && array_key_exists('userName', $_POST)){
        $userName = $_POST['userName'];
    } 
    else if(array_key_exists('user', $_GET)){
         $userName = $_GET['user'];
    }
    else if(array_key_exists('userId', $_SESSION)) {
        require_once './includes/db.php';
        $connection = ShoppingCartDB::getInstance();
        $row = $connection->getUserDetailsById($_SESSION['userId']);
        $userName = $row['first_name'];
    }
    
?><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" rel="stylesheet" type="text/css">
        <title></title>
    </head>
    <body>
        <h1>The Everything Must GO! Store</h1>
        <div id="menu">
            <ul> 
                <li>
                    <form action="cart.php?user=<?php if($userName!=NULL)print$userName;?>" method="POST">
                        <input type="submit" value="My Cart" name="myCart" />
                    </form>
                </li>
                <li>
                    <form action="userLogin.php" method="POST">
                        <input type="submit" value="Log In" name="login" />
                    </form>
                </li>
                <?php if($userName!=NULL) { ?>
                <li>
                    <form action="userPortal.php" method="POST">
                        <input type="submit" value="My Portal" name="portal" />
                    </form>
                </li>
                <li>
                    <form action="#" method="POST">
                        <input type="submit" value="Log Out" name="logOut" />
                    </form>
                </li>
                <?php } ?>
            </ul>
        </div>
        <?php 
            if($userName!=NULL) {
        ?>
            <div id="index_welcomeMessage">
                Welcome <?php print $userName;?>
            </div> 
        
        <?php
            }
            require_once 'includes/db.php';
            
            $conn = ShoppingCartDB::getInstance();
            $result = $conn->getAllItems();
            
            print "<div id=\"index_mainContainer\">";
            
            while (($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) != NULL) {
                
                echo '<div class="index_groupContainer">';
                echo '  <div class="index_image"><img src="images/normal/' . $row['image'] . '.jpg" alt="guitar image" /></div>';            
                echo '  <div class="index_itemId">' . $row['id'] . '</div>';
                echo '  <div class="index_name">' . $row['name'] . '</div>';
                echo '  <div class="index_description">' . $row['description'] . '</div>';
                echo '  <div class="index_price">$' . $row['price'] . '</div>';
                echo '  <form action="addToCart.php" method="POST">';
                echo '      <input type="hidden" value="' . $row['id'] . '" name="itemId" />';
                echo '      <input type="submit" value="Add To Cart" name="addedToCart" />';
                echo '  </form>';
                echo '</div>';
            }
            
            print "</div>";
            mysqli_free_result($result);

            
            mysqli_close($conn);
        ?>
    </body>
</html>
