<?php 
    /* At the checkout page, the customer must be allowed to put in their details
     * 1. Check if the customer is logged in. If not, send the customer to the 
     *    log in page
     * 2. If the customer is logged in, populate the form with any details that 
     *    program can retrive from the database.
     * 3. Check if the user has checked the save my information box
     * 4. Save the user's information
     * 5. Proceed to payment
     * 
     * Another way the user can land on this page is after logging after attempting
     * a checkout. In this case, the user will be sent to the index.php 
     */
    $userInfo = "";
    $shippingInfo = "";
    session_start();
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        
        
        
        if(array_key_exists('userId', $_SESSION)){
            require_once './includes/db.php';
            $connection = ShoppingCartDB::getInstance();
            
            $userInfo = $connection->getUserDetailsById($_SESSION['userId']);
            
            if($userInfo['shipping_address_id'] != NULL) {
                $shippingInfo = $connection->getAddressDetailsById($userInfo['shipping_address_id']);
            }
            else {
                $shippingInfo = NULL;
            }
            
        }
        else {
            header("Location: userLogin.php?illegalAttempt=0"); 
        }
    }
    else if (array_key_exists('userId', $_SESSION)){

        if(array_key_exists('userId', $_SESSION)){
            require_once './includes/db.php';
            $connection = ShoppingCartDB::getInstance();
            
            $userInfo = $connection->getUserDetailsById($_SESSION['userId']);
            
            if($userInfo['shipping_address_id'] != NULL) {
                $shippingInfo = $connection->getAddressDetailsById($userInfo['shipping_address_id']);
            }
            else {
                $shippingInfo = NULL;
            }
            
        }
        else {
            header("Location: userLogin.php?illegalAttempt=0"); 
        }
        
    }
    else {
        header("Location: index.php");
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="style.css" />
        <title></title>
    </head>
    <body>
        <h1>Delivery Details</h1>
        <div id="checkout_welcomeMessage">
            Please confirm your delivery details <?php print $userInfo['first_name'];?>
        </div>
            
        <div id="checkout_deliveryDetails">
            <form method="POST" action="payment.php">
                <label>First Name</label><input type="text" name="firstName" required="" value="<?php print $userInfo['first_name'];?>" /> <br />
                <label>Last Name</label><input type="text" name="lastName" required="" value="<?php print $userInfo['last_name'];?>" /> <br />
                <label>Street Address</label><input type="text" name="streetAddress" required="" value="<?php if($shippingInfo!=NULL) print $shippingInfo['street_address'];?>"/> <br />
                <label>Suburb</label><input type="text" name="suburb" required="" value="<?php if($shippingInfo!=NULL) print $shippingInfo['suburb'];?>"/> <br />
                <label>City</label><input type="text" name="city" required="" value="<?php if($shippingInfo!=NULL) print $shippingInfo['city'];?>"/> <br />
                <label>Postcode</label><input type="text" name="postcode" required="" value="<?php if($shippingInfo!=NULL) print $shippingInfo['postcode'];?>"/> <br />
                <label>Country</label><input type="text" name="country" required="" value="<?php if($shippingInfo!=NULL) print $shippingInfo['country'];?>"/> <br />
                <span id="checkout_save" ><input type="checkbox" value="Save" name="saveCustomerInformation" />Save my information</span> <br />
                <input id="checkout_submit" type="submit" name="continueToPayment" value="Continue to payment" />
            </form>
        </div>
        
        <?php
        
        ?>
    </body>
</html>
