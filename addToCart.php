<?php

    if($_SERVER['REQUEST_METHOD'] !== 'POST') {
        header("Location: index.php");
    }

    require_once 'includes/db.php';
    session_start();
    $conn = ShoppingCartDB::getInstance();
    $orderId = "";
    
    if(array_key_exists('orderId', $_SESSION)) {
        
        $orderId = $_SESSION['orderId'];
        
        $insertSucess = $conn->storeItemsForOrder($_POST['itemId'], $_SESSION['orderId']);
        
        if($insertSucess) {
            header("Location: index.php");
        }
        else {
            header("Location: error.php");
        }
        
    }
    else {
        
        $orderId = $conn->createNewOrder();
        
        $_SESSION['orderId'] = $orderId;
        
        $insertSucess = $conn->storeItemsForOrder($_POST['itemId'], $_SESSION['orderId']);
        
        if($insertSucess) {
            header("Location: index.php");
        }
        else {
            header("Location: error.php");
        }
    }

?>
