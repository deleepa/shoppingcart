-- Add items
INSERT INTO item (`name`, `description`, `image`, `price`)
       VALUES ('Awesome Guitar', 'Grab this awesome guitar at a super low price today!', 'guitar', '199.99');

INSERT INTO item (`name`, `description`, `image`, `price`)
       VALUES ('Signed Football', 'This piece of memorabilia is worth way more than the price stated. Get it today', 'football', '299.99');

INSERT INTO item (`name`, `description`, `image`, `price`)
       VALUES ('Party Balloons', 'Buy a set of 50 birthday party balloons for your loved ones!', 'balloons', '29.83');

INSERT INTO item (`name`, `description`, `image`, `price`)
       VALUES ('Toy car', 'This toy car was owned by none other than the legendary Fernando Alonso himself. Buy a piece of his childhood today!', 'car', '69.49');

INSERT INTO item (`name`, `description`, `image`, `price`)
       VALUES ('Park tree', 'This tree was saved by a bunch of hippes and is now up for sale! Get it now and feel good about yourself!', 'tree', '129.99');

INSERT INTO item (`name`, `description`, `image`, `price`)
       VALUES ('Chinaware', 'Chinaware from China? Yes please!', 'china', '88.99');