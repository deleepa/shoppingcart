-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'customer'
-- This table will store the customer''s details
-- ---

DROP TABLE IF EXISTS `customer`;
		
CREATE TABLE `customer` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `shipping_address_id` INT(11) NULL DEFAULT NULL,
  `billing_address_id` INT(11) NULL DEFAULT NULL,
  `credit_card_id` INT(11) NULL DEFAULT NULL,
  `username` VARCHAR(20) NOT NULL,
  `password` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table will store the customer''s details';

-- ---
-- Table 'credit_card'
-- This table will hold individual credit card information
-- ---

DROP TABLE IF EXISTS `credit_card`;
		
CREATE TABLE `credit_card` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name_on_card` VARCHAR(150) NOT NULL,
  `card_type` VARCHAR(50) NOT NULL,
  `card_number` VARCHAR(50) NOT NULL,
  `security_code` VARCHAR(10) NOT NULL,
  `expiry_date` DATE NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table will hold individual credit card information';

-- ---
-- Table 'address'
-- This table will contain the shipping and billing addresses of each customer
-- ---

DROP TABLE IF EXISTS `address`;
		
CREATE TABLE `address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `street_address` VARCHAR(150) NOT NULL,
  `suburb` VARCHAR(150) NOT NULL,
  `city` VARCHAR(150) NOT NULL,
  `postcode` VARCHAR(50) NOT NULL,
  `country` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table will contain the shipping and billing addresses o';

-- ---
-- Table 'item'
-- This table will contain all the items that will be available to shop
-- ---

DROP TABLE IF EXISTS `item`;
		
CREATE TABLE `item` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  `image` VARCHAR(150) NULL DEFAULT NULL,
  `price` VARCHAR(11) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table will contain all the items that will be available';

-- ---
-- Table 'order'
-- This table will contain the customer ID and order statuses. The status may be SHOPPING, SHIPPED or DELIVERED. Each order must be linked to one customer
-- ---

DROP TABLE IF EXISTS `order`;
		
CREATE TABLE `order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(15) NOT NULL,
  `customer_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table will contain the customer ID and order statuses. ';

-- ---
-- Table 'item_order'
-- This table contains the relationship between items and orders. Each order can only have one item. 
-- ---

DROP TABLE IF EXISTS `item_order`;
		
CREATE TABLE `item_order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `item_id` INT(11) NOT NULL,
  `order_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'This table contains the relationship between items and order';

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `customer` ADD FOREIGN KEY (shipping_address_id) REFERENCES `address` (`id`);
ALTER TABLE `customer` ADD FOREIGN KEY (billing_address_id) REFERENCES `address` (`id`);
ALTER TABLE `customer` ADD FOREIGN KEY (credit_card_id) REFERENCES `credit_card` (`id`);
ALTER TABLE `order` ADD FOREIGN KEY (customer_id) REFERENCES `customer` (`id`);
ALTER TABLE `item_order` ADD FOREIGN KEY (item_id) REFERENCES `item` (`id`);
ALTER TABLE `item_order` ADD FOREIGN KEY (order_id) REFERENCES `order` (`id`);

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `customer` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `credit_card` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `address` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `item` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `order` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `item_order` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `customer` (`id`,`first_name`,`last_name`,`shipping_address_id`,`billing_address_id`,`credit_card_id`,`username`,`password`) VALUES
-- ('','','','','','','','');
-- INSERT INTO `credit_card` (`id`,`name_on_card`,`card_type`,`card_number`,`security_code`,`expiry_date`) VALUES
-- ('','','','','','');
-- INSERT INTO `address` (`id`,`street_address`,`suburb`,`city`,`postcode`,`country`) VALUES
-- ('','','','','','');
-- INSERT INTO `item` (`id`,`name`,`description`,`image`,`price`) VALUES
-- ('','','','','');
-- INSERT INTO `order` (`id`,`status`,`customer_id`) VALUES
-- ('','','');
-- INSERT INTO `item_order` (`id`,`item_id`,`order_id`) VALUES
-- ('','','');

