<?php
    include_once 'includes/db.php';
    $connection = ShoppingCartDB::getInstance();
    
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        session_start();    
//        var_dump($_SESSION);
//        var_dump($_SERVER);
//        var_dump($_POST);
        
        if(array_key_exists('removedFromCart', $_POST)){
            
            $itemId = $_POST['itemId'];
            $item_orderId = $_POST['item_orderId'];

            $result = $connection->deleteItemFromOrder($_SESSION['orderId'], $itemId, $item_orderId);

            if($result) {
                //header("Location: cart.php");
                print "<div id=\"cart_itemRemovedSucess\">An item has been removed.</div>";
            }
            else {
                //header("Location: error.php");
                print "<div id=\"cart_itemRemovedError\">There was an error removing that item.</div>";
            }
        }
       
    } 
    else {
        header("Location: index.php");
    }
        
?><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" type="text/css" rel="stylesheet">
        <title></title>
        
    </head>
    <body>
        <?php
            /* Here the program must print out the contents of the users
             * basket for confirmation purposes.
             * 1. First the program must fetch the current user's orderId
             * 2. After that, the program must check what items exist under the 
             *    current order. 
             * 3. Each of those items must be printed out in the fashion that 
             *    has been outlined in the GUI design.
             * 4. The user can access this page from their portal. Check for 
             *    the user name in the GET and display a welcome message.
             */
            if(array_key_exists('user', $_GET)){
        ?>
                <div id="cart_welcomeMessage">
                    This is your cart, <?php print $_GET['user'];?>
                </div>
        <?php
                
            }
            if(array_key_exists('orderId', $_SESSION)){
                //First the program must fetch the current user's orderId
                $orderId = $_SESSION['orderId'];
                $total = 0;
                //After that, the program must check what items exist under the current order.
                $result = $connection->getItemsByOrderId($orderId);
                //var_dump($result);
                
                print "<div id=\"cart_orderInfo\">
                        <h2>Order information</h2>
                        <p>Items in basket: " . $result->num_rows."</p>
                        <p>Your order number: " . $_SESSION['orderId']."</p>    
                       </div>";
                
                print "<div id=\"cart_itemsContainer\">";
                for ($i = 0; $i < $result->num_rows; $i++) {
                    
                    $itemOrderRow = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    //var_dump($itemOrderRow);
                    $row = $connection->getSingleItemById($itemOrderRow['item_id']);
                    $total += $row['price'];
                    echo '<div class="cart_item">';
                    echo '  <span class="cart_image"><img src="images/thumbs/'.$row['image'].'.jpg" alt="item image"/></span>';        
                    echo '  <span class="cart_name">'.$row['name'].'</span>';        
                    echo '  <span class="cart_price">$'.$row['price'].'</span>';
                    echo '  <span class="removeButton">';
                    echo '      <form action="#" method="POST">';
                    echo '          <input type="hidden" value="' . $itemOrderRow['id'] . '" name="item_orderId" />';
                    echo '          <input type="hidden" value="' . $row['id'] . '" name="itemId" />';
                    echo '          <input type="submit" value="Remove From Cart" name="removedFromCart" />';
                    echo '      </form>';
                    echo '  </span>';
                    echo '</div>';      
                } 
                print"</div>";
                
                print "<div id=\"cart_purchaseInfo\"> 
                        <h2>Total: \$" . sprintf("%.2f",$total) . "</h2>
                        <form action=\"checkout.php\" method=\"POST\">
                            <input type=\"hidden\" value=\"" . $total . "\" name=\"total\" />
                            <input id=\"cart_checkout\" type=\"submit\" value=\"Checkout\" name=\"checkout\" />
                        </form>
                       </div>";
                
                $_SESSION['total'] = $total;
                
            }
            
            else {
        ?>
        <div id="cart_nothingToDisplay"> 
            Your cart  is currently empty. Go back to the <a href="index.php?user=<?php print $_GET['user'];?>">gallery</a> and order something!
        </div>
            
        <?php
            }
        
        ?> 
    </body>
</html>
