<?php

class ShoppingCartDB extends mysqli {

    private static $classInstance = null;
    //db variables
    var $dbName = "shopping_cart";
    var $dbUser = "root";
    var $dbPassword = "delee103";
    var $dbHost = "localhost";

    public static function getInstance() {

        if (!isset(self::$classInstance)) {

            self::$classInstance = new ShoppingCartDB();
        }
        self::$classInstance->escapePOSTParams();
        return self::$classInstance;
    }

    public function __construct() {
        parent::__construct($this->dbHost, $this->dbUser, $this->dbPassword, $this->dbName);
    }

    public function getAllItems() {

        $queryStr = "SELECT `id`, `name`, `description`, `image`, price FROM `item`";
        //$queryStr = $this->escape_string($queryStr);

        $result = $this->query($queryStr);

        return $result;
    }

    public function createNewOrder() {
        $queryStr = "INSERT INTO `order` (`status`) VALUES ('shopping');";

        //$queryStr = $this->escape_string($queryStr);


        $result = $this->query($queryStr);

        if ($result !== FALSE) {
            return $this->insert_id;
        } else {
            return null;
        }
    }

    public function storeItemsForOrder($itemId, $orderId) {

        $queryStr = "INSERT INTO `item_order`(`item_id`, `order_id`) VALUES ('" . $itemId . "', '" . $orderId . "');";
        //$queryStr = $this->escape_string($queryStr);


        $result = $this->query($queryStr);

        if ($result !== FALSE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getItemsByOrderId($orderId) {

        $queryStr = "SELECT * FROM item_order WHERE order_id='" . $orderId . "';";
        //$queryStr = $this->escape_string($queryStr);

        $result = $this->query($queryStr);

        return $result;
    }

    public function getSingleItemById($itemId) {

        $queryStr = "SELECT * FROM item WHERE id='" . $itemId . "';";
        //$queryStr = $this->escape_string($queryStr);

        $result = $this->query($queryStr);

        if ($result != FALSE) {
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            return $row;
        } else {
            return null;
        }
    }

    public function deleteItemFromOrder($orderId, $itemId, $item_orderId) {

        $queryStr = "DELETE FROM item_order WHERE order_id='" . $orderId . "' AND item_id='" . $itemId . "' AND id='" . $item_orderId . "';";
        //$queryStr = $this->escape_string($queryStr);
        return $this->query($queryStr);
    }

    public function checkUserCredentials($username, $password) {

        $queryStr = "SELECT id from customer WHERE username='$username' AND password='$password';";
//        //$queryStr = $this->escape_string($queryStr);

        $result = $this->query($queryStr);

        if ($result != FALSE) {
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

            if ($row != NULL || $row != FALSE) {
                return $row['id'];
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    public function createUserNewAccount($username, $password, $firstName, $lastName) {
        
        $queryStr = "SELECT id FROM customer WHERE username='$username'";
        
        $checkUserExists = mysqli_fetch_array(($this->query($queryStr)), MYSQLI_ASSOC)['id'];
        
        if($checkUserExists == NULL){
            $queryStr2 = "INSERT INTO customer (username, password, first_name, last_name) 
                            VALUES ('" . $username . "', '" . $password . "', '" . $firstName . "', '" . $lastName . "');";
            //$queryStr = $this->escape_string($queryStr);

            $result = $this->query($queryStr2);

            return $result;
        }
        else {
            return NULL;
        }
        
        
    }

    public function getUserIdByUsername($username) {

        $queryStr = "SELECT id FROM customer WHERE username='" . $username . "';";

        //$queryStr = $this->escape_string($queryStr);

        $result = $this->query($queryStr);

        if ($result != FALSE) {

            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

            if ($row != NULL) {
                return $row['id'];
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    public function getUserDetailsById($userId) {

        $queryStr = "SELECT * FROM customer WHERE id='" . $userId . "'";
        //$queryStr = $this->escape_string($queryStr);

        $result = $this->query($queryStr);

        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

        if ($row != NULL) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function insertShippingDetails($streetAddress, $suburb, $city, $postcode, $country, $userId) {

        $queryStr = "INSERT INTO address (street_address, suburb, city, postcode, country) 
                            VALUES ('" . $streetAddress . "', '" . $suburb . "', '" . $city . "', '" . $postcode . "', '" . $country . "');";
        //$queryStr = $this->escape_string($queryStr);

        $result = $this->query($queryStr);

        if ($result) {
            $getLastId = $this->insert_id;

            $queryStr2 = "UPDATE customer SET shipping_address_id='" . $getLastId . "' WHERE id='" . $userId . "';";

            $result = $this->query($queryStr2);

            if ($result) {
                return $result;
            }
        }
    }

    public function updateOrderDetails($orderId, $status, $userId) {

        $queryStr = "UPDATE shopping_cart.`order` SET `status` = '" . $status . "', `customer_id` = " . $userId . " WHERE id = " . $orderId . ";";
        //$queryStr = $this->escape_string($queryStr);

        $result = $this->query($queryStr);

        return $result;
    }

    public function getAddressDetailsById($addressId) {

        $queryStr = "SELECT * FROM address WHERE id='" . $addressId . "';";
        //$queryStr = $this->escape_string($queryStr);

        $result = $this->query($queryStr);

        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

        if ($row != NULL) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getOrdersByUserId($userId) {
        $queryStr = "SELECT * FROM `order` WHERE customer_id='" . $userId . "';";
        //$queryStr = $this->escape_string($queryStr);

        $result = $this->query($queryStr);

        return $result;
    }

    public function escapePOSTParams() {
        global $_POST;
        if (isset($_POST)) {

            foreach ($_POST as $key => $value) {
                $_POST[$key] = $this->escape_string($value);
            }
        }
    }

}

?>
