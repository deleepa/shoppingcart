<?php
    /*There are two ways the user can land on this page
     * First by clicking on the login button on the index.php
     * Second by being forced to log in during checkout.
     * 
     */
    $wrongCredentials = false;
    session_start();
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        if(array_key_exists('userLoginCheck', $_POST)) {
            require_once './includes/db.php';
            $connection = ShoppingCartDB::getInstance();
            
            $result = $connection->checkUserCredentials($_POST['username'], $_POST['password']);
            
            //if second case, there will be an orderId in the session array
            if($result != NULL && array_key_exists('orderId', $_SESSION)) {
                
                $_SESSION['userId'] = $result;
                header("Location: checkout.php");
            }
            else if ($result != NULL && !array_key_exists('orderId', $_SESSION)){
                $_SESSION['userId'] = $result;
                header("Location: userPortal.php");
            }
            else {
                $wrongCredentials = true;
            } 
        }
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" type="text/css" rel="stylesheet" >
        <title></title>
    </head>
    <body>
        <h1>User Login</h1>
        <?php
            if(array_key_exists('illegalAttempt', $_GET)) {
                
        ?>
        
             <div class="userLogin_error">You must be logged in to continue</div>
        
        <?php
            }
        ?>
            <div id="userLogin_loginForm">
                <form action="#" method="POST">
                    <label>Username</label> <input type="text" name="username" value="" /> <br />
                    <label>Password</label> <input type="password" name="password" value="" /> <br />
                    <input id="userLogin_submit" type="submit" value="Submit" name="userLoginCheck"/>
                </form>
                <?php if($wrongCredentials) print "<p>We couldn't find your account</p>";?>
            </div>
             
             <div id="userLogin_register">
                 Don't have an account yet? <a href="newAccount.php">Click here</a> to register now! 
             </div> 
    </body>
</html>
